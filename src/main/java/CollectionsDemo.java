import java.util.*;

public class CollectionsDemo {
    public static int amountOfStringsStartWithSymbol(List<String> list, String symbol){
        int count = 0;
        for (String s: list){
            if (s.startsWith(symbol)){
                count++;
            }
        }
        return count;
    }

    public static List<Human> getListWithSameSecondName(List<Human> humans, Human human){
        List<Human> res = new ArrayList<>();
        for(Human h: humans){
            if(h.getSecondName().equals(human.getSecondName())){
                res.add(h);
            }
        }
        return res;
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает.

    3. Вход: список объектов типа Human и еще один объект типа Human. Выход — копия
    входного списка, не содержащая выделенного человека. При изменении элементов
    входного списка элементы выходного изменяться не должны
    */
    public static List<Human> getHumanListByHumanSecondName(List<Human> humans, Human human){
        List<Human> res = new ArrayList<>();
        for(Human h: humans){
            if(!h.equals(human)){
                res.add(new Human(h.getFirstName(), h.getSecondName(), h.getMiddleName(), h.getAge()));
            }
        }
        return res;
    }

    public static List<Set<Integer>> getNotIntersectedSets(List<Set<Integer>> setList, Set<Integer> set){
        List<Set<Integer>> res = new ArrayList<>();
        for (Set<Integer> s: setList){
            boolean intersect = false;
            for(Integer i: set){
                if (s.contains(i)){
                    intersect = true;
                    break;
                }
            }
            if (!intersect) {
                res.add(s);
            }
        }
        return res;
    }

    public static List<Human> getHumansWithMaxAge(List<Human> humans){
        int maxAge = 0;
        for (Human h: humans){
            if (h.getAge() > maxAge) {
                maxAge = h.getAge();
            }
        }

        List<Human> res = new ArrayList<>();
        for (Human h: humans){
            if (h.getAge() == maxAge) {
                res.add(h);
            }
        }
        return res;
    }

    public static List<Human> getListFromSet(Set<Human> humanSet){
        TreeSet<Human> treeSet = new TreeSet<>((h1, h2)->h1.getFIO().compareTo(h2.getFIO()));
        treeSet.addAll(humanSet);
        return new ArrayList<>(treeSet);
    }

    public static Set<Human> humansByIdentity(Map<Integer, Human> integerHumanMap, Set<Integer> id){
        Set<Human> res = new HashSet<>();
        for (Integer i: id){
            if (integerHumanMap.containsKey(i)){
                res.add(integerHumanMap.get(i));
            }
        }
        return res;
    }

    public static List<Integer> humanIdAboveEighteen(Map<Integer, Human> integerHumanMap){
        List<Integer> res = new ArrayList<>();
        for (Map.Entry<Integer, Human> entry: integerHumanMap.entrySet()){
            if (entry.getValue().getAge() >= 18){
                res.add(entry.getKey());
            }
        }
        return res;
    }

    public static Map<Integer, Integer> getAgeByIdentity(Map<Integer, Human> integerHumanMap){
        Map<Integer, Integer> res = new HashMap<>();
        for (Map.Entry<Integer, Human> entry: integerHumanMap.entrySet()){
            res.put(entry.getKey(), entry.getValue().getAge());
        }
        return res;
    }

    public static Map<Integer, List<Human>> getAgeHumanMap(Set<Human> humans){
        Map<Integer, List<Human>> res = new HashMap<>();
        for (Human h: humans){
            if (!res.containsKey(h.getAge())){
                res.put(h.getAge(), new ArrayList<Human>());
            }
            res.get(h.getAge()).add(h);
        }
        return res;
    }

    public static Map<Integer, Map<String, List<Human>>> getAgeSecondNameHumanMap(Set<Human> humans){
        Map<Integer, Map<String, List<Human>>> res = new HashMap<>();
        for (Human h: humans){
            if (!res.containsKey(h.getAge())){
                res.put(h.getAge(), new HashMap<String, List<Human>>());
            }
            if(!res.get(h.getAge()).containsKey(h.getSecondName().substring(0, 1))){
                res.get(h.getAge()).put(h.getSecondName().substring(0, 1), new ArrayList<Human>());
            }
            res.get(h.getAge()).get(h.getSecondName().substring(0, 1)).add(h);
        }
        return res;
    }
}

package errors;

public class PhoneBookException extends Exception{
    PhoneBookErrorCode error;

    public PhoneBookException(PhoneBookErrorCode error) {
        this.error = error;
    }
}

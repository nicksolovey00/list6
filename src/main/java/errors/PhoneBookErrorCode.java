package errors;

public enum PhoneBookErrorCode {
    NO_SUCH_NUMBER("This number was not added"),
    INAPPROPRIATE_NUMBER("Number consists letters or other symbols that does't suit"),
    NO_SUCH_HUMAN("There no such human in phonebook");

    private String errorCode;

    PhoneBookErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}

import java.util.Comparator;

public class HumanComparator implements Comparator<Human> {
    @Override
    public int compare(Human human1, Human human2) {
        return human1.getFIO().compareTo(human2.getFIO());
    }
}

package Special;

import java.util.ArrayList;
import java.util.List;

public class DataDemo {
    public static List<Integer> getAll(Data data){
        List<Integer> res = new ArrayList<>();
        while(data.iterator().hasNext()){
            res.add(data.iterator().next());
        }
        return res;
    }
}

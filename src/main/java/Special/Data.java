package Special;

import java.util.Iterator;
import java.util.List;

public class Data implements Iterable<Integer> {
    private String name;
    Group[] groups;
    private int currentElement;
    private int currentGroup;

    public Data(String name, Group... groups) {
        this.name = name;
        this.groups = new Group[groups.length];
        setGroups(groups);
        currentElement = 0;
        currentGroup = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        for (int i = 0; i < this.groups.length; i++) {
            this.groups[i] = new Group(groups[i].getId(), groups[i].getData());
        }
    }

    public int length() {
        return groups.length;
    }


    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return currentGroup < groups.length;
            }

            @Override
            public Integer next() {
                int res = groups[currentGroup].getData()[currentElement];
                currentElement++;
                if(currentElement >= groups[currentGroup].getData().length){
                    currentGroup++;
                    currentElement = 0;
                }
                return res;
            }
            @Override
            public void remove() {
                return;
            }
        };
    }
}


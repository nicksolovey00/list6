package Special;

public class Group {
    private int id;
    private int[] data;

    public Group(int id, int ... data) {
        this.id = id;
        this.data = new int[data.length];
        setData(data);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        for (int i = 0; i < this.data.length; i++){
            this.data[i] = data[i];
        }
    }

    public int length(){
        return data.length;
    }
}

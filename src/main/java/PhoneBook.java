import errors.PhoneBookErrorCode;
import errors.PhoneBookException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PhoneBook {
    private Map<Human, List<String>> phoneBook;

    public PhoneBook(){
        phoneBook = new HashMap<>();
    }

    public Map<Human, List<String>> getPhoneBook() {
        return phoneBook;
    }

    private void checkNumber(String number) throws PhoneBookException {
        if(number.length() != 12){
            throw new PhoneBookException(PhoneBookErrorCode.INAPPROPRIATE_NUMBER);
        }
        for(int i = 0; i < number.length(); i++){
            if((number.charAt(i) < 48 || number.charAt(i) > 57) && number.charAt(i) != 43){
                throw new PhoneBookException(PhoneBookErrorCode.INAPPROPRIATE_NUMBER);
            }
        }
    }

    public void addNumber(Human human, String number) throws PhoneBookException {
        checkNumber(number);
        if(!phoneBook.containsKey(human)){
            phoneBook.put(human, new LinkedList<String>());
        }
        phoneBook.get(human).add(number);
    }

    public void deleteNumber(Human human, String number) throws PhoneBookException {
        if(!phoneBook.containsKey(human)){
            throw new PhoneBookException(PhoneBookErrorCode.NO_SUCH_HUMAN);
        }
        if(!phoneBook.get(human).contains(number)){
            throw new PhoneBookException(PhoneBookErrorCode.NO_SUCH_NUMBER);
        }
        phoneBook.get(human).remove(number);

        if (phoneBook.get(human).isEmpty()){
            phoneBook.remove(human);
        }
    }

    public List<String> getHumanNumbers(Human human) throws PhoneBookException {
        if (!phoneBook.containsKey(human)){
            throw new PhoneBookException(PhoneBookErrorCode.NO_SUCH_HUMAN);
        }
        return phoneBook.get(human);
    }

    public Human findHumanByNumber(String number) throws PhoneBookException {
        checkNumber(number);
        for(Map.Entry<Human, List<String>> entry: phoneBook.entrySet()){
            if(entry.getValue().contains(number)){
                return entry.getKey();
            }
        }
        throw new PhoneBookException(PhoneBookErrorCode.NO_SUCH_HUMAN);
    }

    public Map<Human, List<String>> getRelevantMap(String secondNameBeginning){
        Map<Human, List<String>> res = new HashMap<>();
        for(Map.Entry<Human, List<String>> entry: phoneBook.entrySet()){
            if(entry.getKey().getSecondName().startsWith(secondNameBeginning)){
                res.put(entry.getKey(), entry.getValue());
            }
        }
        return res;
    }
}

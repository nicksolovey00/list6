import errors.PhoneBookException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PhoneBookTest {

    @Test
    public void testDeleteNumber() throws PhoneBookException {
        PhoneBook phoneBook = new PhoneBook();
        Human human = new Human("Mark", "Manson", "", 34);

        phoneBook.addNumber(human,"+79131469965");
        phoneBook.addNumber(human,"+79136789574");
        phoneBook.deleteNumber(human, "+79131469965");

        List<String> rightAnswer = new LinkedList<>();
        rightAnswer.add("+79136789574");

        Assert.assertEquals(rightAnswer, phoneBook.getHumanNumbers(human));
    }

    @Test
    public void testAddNumber() throws PhoneBookException {
        PhoneBook phoneBook = new PhoneBook();
        Human human = new Human("Mark", "Manson", "", 34);
        phoneBook.addNumber(human,"+79131469965");
        phoneBook.addNumber(human,"+79136789574");

        List<String> rightAnswer = new LinkedList<>();
        rightAnswer.add("+79131469965");
        rightAnswer.add("+79136789574");

        Assert.assertEquals(rightAnswer, phoneBook.getHumanNumbers(human));
    }

    @Test
    public void testFindHumanByNumber() throws PhoneBookException {
        PhoneBook phoneBook = new PhoneBook();
        Human human1 = new Human("Mark", "Manson", "", 34);
        Human human2 = new Human("Lisa", "Huston", "", 27);
        phoneBook.addNumber(human1,"+79131469965");
        phoneBook.addNumber(human1,"+79136789574");
        phoneBook.addNumber(human2,"+75847839392");

        Assert.assertEquals(human1, phoneBook.findHumanByNumber("+79136789574"));
        Assert.assertEquals(human2, phoneBook.findHumanByNumber("+75847839392"));
    }

    @Test
    public void testGetRelevantMap() throws PhoneBookException {
        PhoneBook phoneBook = new PhoneBook();
        Human human1 = new Human("Mark", "Handsome", "", 34);
        Human human2 = new Human("Lisa", "Huston", "", 27);
        Human human3 = new Human("Mary", "Huston", "", 22);

        phoneBook.addNumber(human1,"+79131469965");
        phoneBook.addNumber(human2,"+76374896967");
        phoneBook.addNumber(human3,"+76574302876");

        Map<Human, List<String>> rightAnswer = new HashMap<>();
        rightAnswer.put(human2, new LinkedList<String>());
        rightAnswer.put(human3, new LinkedList<String>());
        rightAnswer.get(human2).add("+76374896967");
        rightAnswer.get(human3).add("+76574302876");

        Assert.assertEquals(phoneBook.getPhoneBook(), phoneBook.getRelevantMap("H"));
        Assert.assertEquals(rightAnswer, phoneBook.getRelevantMap("Hu"));
    }
}

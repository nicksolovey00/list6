import Special.Data;
import Special.DataDemo;
import Special.Group;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestSpecial {

    @Test
    public void testGetAll(){
        Group group1 = new Group(1, 1, 2, 3, 4);
        Group group2 = new Group(2, 5, 6, 7);
        Group group3 = new Group(3, 8, 9, 10, 11, 12, 13, 14, 15);
        Data data = new Data("Groups 1-3", group1, group2, group3);

        List<Integer> expected = new ArrayList<>();

        for(int i = 1; i <= 15; i++){
            expected.add(i);
        }

        Assert.assertEquals(expected, DataDemo.getAll(data));
    }
}

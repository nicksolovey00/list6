import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class TestCollectionsDemo {
    @Test
    public void testAmountOfStringsStartWithSymbol(){
        List<String> list = new ArrayList<>();
        list.add("Hello");
        list.add("July");
        list.add("Hero");
        Assert.assertEquals(2, CollectionsDemo.amountOfStringsStartWithSymbol(list, "H"));
        Assert.assertEquals(0, CollectionsDemo.amountOfStringsStartWithSymbol(list, "W"));
    }

    @Test
    public void testGetListWithSameSecondName(){
        List<Human> list = new ArrayList<>();
        list.add(new Human("Andy", "Willson", "", 13));
        list.add(new Human("Martin", "Willson", "", 15));
        list.add(new Human("Jack", "Stauber", "", 19));

        List<Human> rightAnswer = new ArrayList<>();
        rightAnswer.add(list.get(0));
        rightAnswer.add(list.get(1));

        Assert.assertEquals(rightAnswer, CollectionsDemo.getListWithSameSecondName(list, new Human("", "Willson", "", 10)));
    }

    @Test
    public void testGetHumanListByHumanSecondName(){
        List<Human> list = new ArrayList<>();
        list.add(new Human("Andy", "Willson", "", 13));
        list.add(new Human("Martin", "Willson", "", 15));
        list.add(new Human("Jack", "Stauber", "", 19));

        List<Human> rightAnswer = new ArrayList<>();
        rightAnswer.add(new Human("Andy", "Willson", "", 13));
        rightAnswer.add(new Human("Martin", "Willson", "", 15));

        List<Human> answer = CollectionsDemo.getHumanListByHumanSecondName(list, new Human("Jack", "Stauber", "", 19));

        Assert.assertEquals(rightAnswer, answer);

        list.get(0).setSecondName("aaaaaaaaaaaa");
        Assert.assertEquals(rightAnswer, answer);
    }

    @Test
    public void testGetNotIntersectedSets(){
        List<Set<Integer>> list = new ArrayList<>();
        list.add(new HashSet<Integer>());
        list.add(new HashSet<Integer>());
        list.add(new HashSet<Integer>());
        list.get(1).add(1);
        list.get(2).add(2);
        list.get(2).add(3);

        HashSet<Integer> set = new HashSet<>();
        set.add(1);

        List<Set<Integer>> rightAnswer = new ArrayList<>();
        rightAnswer.add(list.get(0));
        rightAnswer.add(list.get(2));

        Assert.assertEquals(rightAnswer, CollectionsDemo.getNotIntersectedSets(list, set));
    }

    @Test
    public void testGetHumansWithMaxAge(){
        List<Human> list = new ArrayList<>();
        list.add(new Human("","", "", 15));
        list.add(new Students("Fiona", "", "", 19, ""));
        list.add(new Students("Kelly", "", "", 19, ""));
        list.add(new Human("", "", "", 17));

        List<Human> rightAnswer = new ArrayList<>();
        rightAnswer.add(list.get(1));
        rightAnswer.add(list.get(2));

        Assert.assertEquals(rightAnswer, CollectionsDemo.getHumansWithMaxAge(list));
    }

    @Test
    public void testGetListFromTreeSet(){
        Set<Human> humanSet = new HashSet<>();
        Human human1 = new Students("A", "A", "", 15, "");
        Human human2 = new Human("B", "B", "", 15);
        Human human3 = new Human("B", "A", "", 15);
        Human human4 = new Human("A", "B", "", 15);

        Collections.addAll(humanSet, human1, human2, human3, human4);

        ArrayList<Human> exp = new ArrayList<>();
        exp.add(human1);
        exp.add(human3);
        exp.add(human4);
        exp.add(human2);

        Assert.assertEquals(exp, CollectionsDemo.getListFromSet(humanSet));
    }

    @Test
    public void testHumansByIdentity(){
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("", "", "", 14));
        map.put(2, new Human("", "", "", 15));
        map.put(3, new Human("", "", "", 16));
        map.put(4, new Human("", "", "", 17));

        Set<Integer> set = new HashSet<>();
        set.add(2);
        set.add(3);

        Set<Human> rightAnswer = new HashSet<>();
        rightAnswer.add(map.get(2));
        rightAnswer.add(map.get(3));

        Assert.assertEquals(rightAnswer, CollectionsDemo.humansByIdentity(map, set));
    }

    @Test
    public void testHumanIdAboveEighteen(){
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("", "", "", 25));
        map.put(2, new Human("", "", "", 15));
        map.put(3, new Human("", "", "", 37));
        map.put(4, new Human("", "", "", 17));

        List<Integer> rightAnswer = new ArrayList<>();
        rightAnswer.add(1);
        rightAnswer.add(3);

        Assert.assertEquals(rightAnswer, CollectionsDemo.humanIdAboveEighteen(map));
    }

    @Test
    public void testGetAgeByIdentity(){
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("", "", "", 25));
        map.put(2, new Human("", "", "", 15));
        map.put(3, new Human("", "", "", 37));
        map.put(4, new Human("", "", "", 17));

        Map<Integer, Integer> rightAnswer = new HashMap<>();
        rightAnswer.put(1, 25);
        rightAnswer.put(2, 15);
        rightAnswer.put(3, 37);
        rightAnswer.put(4, 17);

        Assert.assertEquals(rightAnswer, CollectionsDemo.getAgeByIdentity(map));
    }

    @Test
    public void testGetAgeHumanMap(){
        Set<Human> set = new HashSet<>();
        set.add(new Human("Andy", "", "", 17));
        set.add(new Human("Mary", "", "", 17));
        set.add(new Human("Mark", "", "", 23));
        set.add(new Human("Veronica", "", "", 21));

        Map<Integer, List<Human>> rightAnswer = new HashMap<>();
        rightAnswer.put(17, new ArrayList<Human>());
        rightAnswer.put(23, new ArrayList<Human>());
        rightAnswer.put(21, new ArrayList<Human>());
        rightAnswer.get(17).add(new Human("Andy", "", "", 17));
        rightAnswer.get(17).add(new Human("Mary", "", "", 17));
        rightAnswer.get(23).add(new Human("Mark", "", "", 23));
        rightAnswer.get(21).add(new Human("Veronica", "", "", 21));

        Assert.assertEquals(rightAnswer, CollectionsDemo.getAgeHumanMap(set));
    }

    @Test
    public void testGetAgeSecondNameMap(){
        Set<Human> set = new HashSet<>();
        set.add(new Human("Andy", "A", "", 17));
        set.add(new Human("Mary", "A", "", 17));
        set.add(new Human("Mary", "D", "", 17));
        set.add(new Human("Mark", "B", "", 23));
        set.add(new Human("Veronica", "C", "", 21));

        System.out.println(CollectionsDemo.getAgeSecondNameHumanMap(set));
    }
}
